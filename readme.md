Timer - creates future which will resolve in certain time period.

JobStream - stream executing jobs per given time interval and yielding job results. Job could decide on whether it is done or reschedule itself or another job.

UdpSocketWraper - provides asycronius behavior for UDP sockets.