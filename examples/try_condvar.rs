#![allow(unused)]

use std::task::{Poll, Context};
use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc, Mutex, Condvar};
use std::ops::DerefMut;
use futures::executor::block_on;
use std::thread;
use std::time::Duration;
use std::thread::JoinHandle;
use async_job::timer::timer_future::{TimerFutureStateSafe, TimerFuture};


fn main() {
    let queue = Arc::new(Mutex::new((Vec::new(), false)));
    let time_future1 = TimerFuture::new(Duration::from_secs(1));
    queue.lock().unwrap().0.push(time_future1.clone_state());
    let time_future2 = TimerFuture::new(Duration::from_secs(1));
    queue.lock().unwrap().0.push(time_future2.clone_state());

    let condvar = Arc::new(Condvar::new());
    let pair2 = (queue.clone(), condvar.clone());
    let join_handle = launch(queue, condvar);

    let (lock, cvar) = pair2;
    {
        let mut guard = lock.lock().unwrap();
        eprintln!("interrupting");
        guard.1 = true;
        cvar.notify_one();
    }


    let x = async {
        eprintln!("before await");
        time_future1.await;
        eprintln!("after await");
    };
    block_on(x);
}

fn launch(lock: Arc<Mutex<(Vec<TimerFutureStateSafe>, bool)>>, cvar: Arc<Condvar>) -> JoinHandle<()>{

     thread::spawn(move || {
        // wait for the thread to start up
        let mut guard = lock.lock().unwrap();
        let duration;
        {
            duration = guard.0[0].lock().unwrap().duration;
        }
// as long as the value inside the `Mutex<bool>` is `false`, we wait
        loop {
            if guard.1 == true {
                eprintln!("got interrupted signal, exit thread");
                // We received the notification and the value has been updated, we can leave.
                break;
            }

            let result = cvar.wait_timeout(guard, Duration::from_millis(1000)).unwrap();
            // 10 milliseconds have passed, or maybe the value changed!
            guard = result.0;
            if result.1.timed_out() {
                let mut state = guard.0[0].lock().unwrap();
                state.completed = true;
                if let Some(waker) = state.waker.take() {
                    waker.wake()
                }
                break;
            }
        }
    })
}

