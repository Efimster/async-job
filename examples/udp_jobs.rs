//TODO: The tasks are taken sequentially per TA interval. Once taken tasks are executing concurrently.
// Task performs io::Read operation. Task result is Result type.
// If task failed with io::WouldWait result, it needs to be combined with a Timer and get back to the queue.
// .
#![recursion_limit="256"]
use futures::{future::FutureExt, stream::{FuturesUnordered, StreamExt}, select, AsyncRead, AsyncReadExt};
use std::{io, time::{Duration, Instant}};
use futures::executor::block_on;
use async_job::timer::timer::Timer;
use std::fmt::Debug;
use async_job::net::udp_socket_wrapper::UdpSocketWrapper;
use async_job::net::io_event_listener::IOEventListener;
use std::io::{Cursor, Write};
use futures::io::AllowStdIo;
use async_job::interval::interval_stream::IntervalStream;
use async_job::interval::job_spawner::Job;
use async_job::interval::job_result::JobResult;
use async_job::traits::transport::ITransport;

fn main() -> io::Result<()>{

    let timer = Timer::new_thread_alive();
    let io_event_listener = IOEventListener::new()?;
    // let interval_timer = IntervalStream::new(Duration::from_millis(1000));
    let interval_timer = IntervalStream::with_timer(timer.clone(), Duration::from_millis(400));

    let mut udp = UdpSocketWrapper::bind_and_connect("127.0.0.1:3400",
        "127.0.0.1:3478", io_event_listener.clone()).unwrap();
    let mut udp2 = UdpSocketWrapper::bind_and_connect("127.0.0.1:3401",
        "127.0.0.1:3479", io_event_listener.clone()).unwrap();

    udp.set_read_timeout(Some(Duration::from_secs(2)))?;

    let bytes = [0x00u8, 0x01, 0x00, 0x00, 0x21, 0x12, 0xa4, 0x42, 0x73, 0x39, 0x4d, 0x4b, 0x73, 0x71, 0x36, 0x51, 0x54, 0x46, 0x6c, 0x48];
    Write::write(&mut udp, &bytes)?;
    Write::write(&mut udp2, &bytes)?;
    let mut queue = Vec::new();
    // queue.push(job(0, timer.clone(), 2));
    // queue.push(job(1, timer.clone(), 2));
    let cursor = AllowStdIo::new(Cursor::new([1u8, 2, 3, 4,5]));
    let start = Instant::now();
    queue.push(job(0, udp2, timer.clone(), 2, start));
    queue.push(job(1, cursor, timer.clone(), 2, start));
    queue.push(job(2, udp, timer.clone(), 2, start));

    let x = block_on(run_loop(interval_timer, queue, start));
    eprintln!("{:?}", x);
    Ok(())
}

fn job<I>(id:usize, mut readable:I, timer: Timer, retry_count:usize, start:Instant) -> Job<Box<[u8]>, ()>
    where I: AsyncRead + Unpin + Send + 'static,
{
    let mut buf = [0u8; 1500];
    let fut = async move {
        eprintln!("{:?} try (attempts left {}) read data from udp socket...", start.elapsed(), retry_count);
        match readable.read(&mut buf).await {
            Err(ref err) if matches!(err.kind(), io::ErrorKind::WouldBlock) && retry_count > 0 =>{
                let mut timer_clone = timer.clone();
                let job = async move {
                    eprintln!("{:?} awaiting", start.elapsed());
                    timer_clone.wait(Duration::from_millis(1000)).await;
                    job(id, readable, timer, retry_count - 1, start).await
                };
                JobResult::Run{id, job:job.boxed()}
            }

            Err(_) => JobResult::Fail{id, err:()},
            Ok(size) => JobResult::Success{id, result:Box::from(&buf[..size])},
        }
    };
    fut.boxed()
}

// fn job(id: usize, mut timer: Timer, retry_count: usize) -> Job<bool>
// {
//     let fut = async move {
//         eprintln!("try ({}) sleep...", retry_count);
//         timer.wait(Duration::from_millis(2000)).await;
//
//         if retry_count > 0 {
//             let mut timer_clone = timer.clone();
//             let job = async move {
//                 //eprintln!("awaiting");
//                 timer_clone.wait(Duration::from_millis(1000)).await;
//                 job(id, timer, retry_count - 1).await
//             };
//             JobResult::Run(job.boxed())
//         }
//         else {
//             JobResult::Success(true)
//         }
//     };
//     fut.boxed()
// }

async fn run_loop<R:Debug>(mut interval_timer: IntervalStream, mut queue: Vec<Job<R, ()>>, start:Instant) -> Vec<JobResult<R, ()>> {
    let mut job_results:Vec<JobResult<R, ()>> = Vec::with_capacity(queue.len());
    let mut job_left = queue.len();
    let mut job_set = FuturesUnordered::new();

    loop {
        select! {
            () = interval_timer.select_next_some() => {
                eprintln!("{:?} ({}) TA", start.elapsed(), queue.len());
                match queue.pop().take() {
                    Some(job) => job_set.push(job),
                    None => {
                        interval_timer.interrupt();
                    }
                }
            },
            job_result = job_set.select_next_some() => {
                match job_result {
                    JobResult::Run{id, job} => {
                        eprintln!("{:?} Retry {}", start.elapsed(), id);
                        queue.insert(0,job);
                        interval_timer.resume();
                    },
                    JobResult::Fail{id, err:_} => {
                        eprintln!("{:?} Failed {}", start.elapsed(), id);
                        job_results.push(job_result);
                        job_left -= 1;
                    },
                    JobResult::Success{id, result} => {
                        eprintln!("{:?} Success {} {:?}", start.elapsed(), id, result);
                        job_results.push(JobResult::Success{id, result});
                        job_left -= 1;
                    }
                }
                if job_left == 0 {
                    job_results.sort_by(|a, b|{a.id().cmp(&b.id())});
                    return job_results;
                }
            }
        }
    }
}

