//TODO: The tasks are taken sequentially per TA interval. Once taken tasks are executing concurrently.
// Task performs io::Read operation. Task result is Result type.
// If task failed with io::WouldWait result, it needs to be combined with a Timer and get back to the queue.
// .
#![recursion_limit = "256"]

use futures::{future::FutureExt, AsyncRead, AsyncReadExt, StreamExt};
use std::time::{Duration, Instant};
use std::io;
use async_job::timer::timer::Timer;
use async_job::net::udp_socket_wrapper::UdpSocketWrapper;
use async_job::net::io_event_listener::IOEventListener;
use std::io::{Cursor, Write};
use futures::io::AllowStdIo;
use async_job::interval::job_spawner::Job;
use async_job::interval::job_result::JobResult;
use futures::executor::block_on;
use async_job::interval::job_stream::JobStream;
use async_job::traits::transport::ITransport;

fn main() -> io::Result<()> {
    let timer = Timer::new_thread_alive();

    let io_event_listener = IOEventListener::new()?;
    let mut udp = UdpSocketWrapper::bind_and_connect("127.0.0.1:3400",
 "127.0.0.1:3478", io_event_listener.clone()).unwrap();
    let mut udp2 = UdpSocketWrapper::bind_and_connect("127.0.0.1:3401",
 "127.0.0.1:3479", io_event_listener.clone()).unwrap();

    udp.set_read_timeout(Some(Duration::from_secs(2)))?;

    let bytes = [0x00u8, 0x01, 0x00, 0x00, 0x21, 0x12, 0xa4, 0x42, 0x73, 0x39, 0x4d, 0x4b, 0x73, 0x71, 0x36, 0x51, 0x54, 0x46, 0x6c, 0x48];
    Write::write(&mut udp, &bytes)?;
    Write::write(&mut udp2, &bytes)?;
    let cursor = AllowStdIo::new(Cursor::new([1u8, 2, 3, 4, 5]));
    let start = Instant::now();

    let (job_stream, mut spawner) =
        JobStream::new_with_timer(Duration::from_millis(400), timer.clone());

    spawner.spawn(job(0, udp, timer.clone(), 2, start));
    spawner.spawn(job(1, cursor, timer.clone(), 2, start));
    spawner.spawn(job(2, udp2, timer.clone(), 2, start));
    drop(spawner);

    block_on(job_stream.for_each(|result| {
        async move{
            eprintln!("{:?} {:?}", start.elapsed(), result);
        }
    }));

    // let mut job_results = block_on(job_stream.collect::<Vec<_>>());
    // job_results.sort_by(|a, b| { a.id().cmp(&b.id()) });
    // job_results.iter().for_each(|result| eprintln!("{:?} {:?}", start.elapsed(), result));

    Ok(())
}

fn job<I>(id: usize, mut readable: I, timer: Timer, retry_count: usize, start:Instant) -> Job<Box<[u8]>, ()>
    where I: AsyncRead + Unpin + Send + 'static,
{
    let mut buf = [0u8; 1500];
    let fut = async move {
        eprintln!("{:?} try (attempts left {}) read data from udp socket...", start.elapsed(), retry_count);
        match readable.read(&mut buf).await {
            Err(ref err) if matches!(err.kind(), io::ErrorKind::WouldBlock) && retry_count > 0 => {
                let mut timer_clone = timer.clone();
                let job = async move {
                    eprintln!("{:?} awaiting", start.elapsed());
                    timer_clone.wait(Duration::from_millis(1000)).await;
                    job(id, readable, timer, retry_count - 1, start).await
                };
                JobResult::Run { id, job: job.boxed() }
            }

            Err(_) => JobResult::Fail { id, err: () },
            Ok(size) => JobResult::Success { id, result: Box::from(&buf[..size]) },
        }
    };
    fut.boxed()
}



