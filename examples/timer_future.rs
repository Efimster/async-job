#![allow(unused)]

use std::task::{Poll, Context};
use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc, Mutex, Condvar};
use std::ops::DerefMut;
use futures::executor::block_on;
use std::thread;
use std::time::Duration;
use std::thread::JoinHandle;
use futures::join;
use async_job::timer::timer::Timer;


fn main() {
    let mut timer = Timer::new();
    let test = async {
        //for i in 0..2 {
            let x1 = timer.wait(Duration::from_millis(1000));
            thread::sleep(Duration::from_millis(450));
            let x2 = timer.wait(Duration::from_millis(500));
            // thread::sleep(Duration::from_millis(1000));
            let x3 = timer.wait(Duration::from_millis(1000));

            join!(x1, x2, x3);
        //}

    };
    block_on(test);
    eprintln!("unblock main");
}
