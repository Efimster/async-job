use std::sync::{Arc, Mutex};
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use futures::future::FusedFuture;

use crate::common::ready_future_state::ReadyFutureState;

use super::ready_future_state::ReadyFutureResult;

pub type ReadyFutureStateSafe<T> = Arc<Mutex<ReadyFutureState<T>>>;

pub struct ReadyFuture<T> {
    shared_state: ReadyFutureStateSafe<T>,
}

impl<T> ReadyFuture<T> {
    pub fn new() -> Self {
        Self::with_shared_state(Arc::new(Mutex::new(ReadyFutureState::new())))
    }

    pub fn with_shared_state(shared_state: ReadyFutureStateSafe<T>) -> Self {
        ReadyFuture { shared_state }
    }

    pub fn clone_state(&self) -> ReadyFutureStateSafe<T> {
        self.shared_state.clone()
    }

    pub fn get_state(&self) -> std::sync::MutexGuard<'_, ReadyFutureState<T>> {
        self.shared_state.lock().unwrap()
    }

    pub fn new_resolved(val:T) -> Self {
        let result = Self::new();
        result.get_state().fulfill(val);
        result
    }

    pub fn terminate(&self) {
        self.get_state().terminate()
    }
}

impl<T:Clone> Future for ReadyFuture<T> {
    type Output = ReadyFutureResult<T>;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // Look at the shared state to see if the timer has already completed.
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.is_fulfilled() {
            // eprintln!("completed, timeout={}", shared_state.timeout);
            let result = shared_state.result.clone().take().unwrap();
            Poll::Ready(result)
        } else {
            shared_state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

impl<T> Clone for ReadyFuture<T> {
    fn clone(&self) -> Self {
        ReadyFuture {
            shared_state: self.clone_state(),
        }
    }
}

impl<T:Clone> FusedFuture for ReadyFuture<T> {
    fn is_terminated(&self) -> bool {
        self.shared_state.lock().unwrap().is_terminated()
    }
}

#[cfg(test)]
mod test {
    use futures::{executor::block_on, select};
    use wasm_bindgen_test::wasm_bindgen_test;

    use super::*;

    #[test]
    #[wasm_bindgen_test]
    fn test_new_fulfilled() {
        let f = ReadyFuture::new_resolved(1_usize);
        let result = block_on(f.clone());
        assert!(matches!(result, ReadyFutureResult::Completed(1)));  
        match result {
            ReadyFutureResult::Completed(result) => assert_eq!(result, 1),
            _ => unreachable!()
        }  

        let result = block_on(f);
        assert!(matches!(result, ReadyFutureResult::Completed(1)));  
        match result {
            ReadyFutureResult::Completed(result) => assert_eq!(result, 1),
            _ => unreachable!()
        }  
    }

    #[test]
    #[wasm_bindgen_test]
    fn test_terminated() {
        let mut f = ReadyFuture::new_resolved(1_usize);
        block_on(async {
            let mut result= 0;
            for i in 0_usize .. 10 {
                result = select! {_ = f => {i}};
            }
            assert_eq!(result, 9);
        });

        let result = block_on(async {
            let mut result= 0;
            for i in 0_usize .. 10 {
                result = select! {_ = f => {i}};
            }
            assert_eq!(result, 9);
            f.terminate();
            select! {
                _ = f => {100},
                complete => 200_usize
            }
        });
        assert_eq!(result, 200);
    }
}
