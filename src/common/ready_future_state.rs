use std::task::Waker;

pub struct ReadyFutureState<T> {
    pub(crate) waker: Option<Waker>,
    pub result: Option<ReadyFutureResult<T>>,
    terminated:bool,
}

impl<T> ReadyFutureState<T> {
    pub fn new() -> ReadyFutureState<T> {
        ReadyFutureState {
            waker: None,
            result: None,
            terminated: false
        }
    }

    pub fn is_fulfilled(&self) -> bool {
        matches!(self.result, Some(_))
    }

    pub fn is_aborted(&self) -> bool {
        matches!(self.result, Some(ReadyFutureResult::Aborted))
    }

    pub fn is_timeouted(&self) -> bool {
        matches!(self.result, Some(ReadyFutureResult::Timeout))
    }

    pub fn is_terminated(&self) -> bool {
        self.terminated
    }

    pub fn abort(&mut self){
        self.result = Some(ReadyFutureResult::Aborted);
        self.wake();
    }

    pub fn timeout(&mut self){
        self.result = Some(ReadyFutureResult::Timeout);
        self.wake();
    }

    pub fn fulfill(&mut self, val:T){
        self.result = Some(ReadyFutureResult::Completed(val));
        self.wake();
    }

    pub(crate) fn wake(&mut self){
        if let Some(waker) = self.waker.take() {
            waker.wake()
        }
    }

    pub fn terminate(&mut self){
        self.terminated = true;
    }
}

#[derive(Clone, Debug)]
pub enum ReadyFutureResult<T> {
    Completed(T),
    Timeout,
    Aborted,
}