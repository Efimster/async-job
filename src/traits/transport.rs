use std::net::SocketAddr;
use std::time::Duration;
use std::io;
use crate::net::read_from_socket::ReadFromSocket;
use std::task::{Context, Poll};

pub trait ITransport {
    fn send_to(&mut self, buf: &[u8], target: SocketAddr) -> io::Result<usize>;
    fn set_read_timeout(&mut self, dur: Option<Duration>) -> io::Result<()>;
    fn recv_from_async<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadFromSocket<'a, Self>
        where Self: std::marker::Sized;
    fn abort_recv(&mut self);
    fn local_addr(&self) -> io::Result<SocketAddr>;
    fn poll_read_attempt(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<(usize, SocketAddr)>>;
}