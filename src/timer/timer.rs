use std::sync::{Arc, Condvar, Mutex};
use std::time::Duration;
use std::thread;
use crate::timer::timer_state::TimerState;
use crate::timer::timer_future::TimerFuture;

pub struct Timer {
    state: Arc<Mutex<TimerState>>,
    condvar: Arc<Condvar>,
    thread_alive: bool,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            state: Arc::new(Mutex::new(TimerState::new())),
            condvar: Arc::new(Condvar::new()),
            thread_alive: false,
        }
    }

    pub fn new_thread_alive() -> Timer {
        let mut new = Timer::new();
        new.thread_alive = true;
        new
    }

    pub fn wait(&mut self, duration: Duration) -> TimerFuture {
        let state = &mut *self.state.lock().unwrap();
        let (time_future, shortest) = state.add_to_queue(duration);
        let launched = state.is_launched();
        if shortest && launched {
            state.interrupt();
            self.condvar.notify_one();
        }
        else if !launched {
            state.launch();
            self.launch();
        }

        time_future
    }

    // pub fn decrement_durations(&mut self, duration: Duration){
    //     let state = &mut *self.state.lock().unwrap();
    //     state.decrement_durations(duration);
    //     state.interrupt();
    //     self.condvar.notify_one();
    // }
    //
    pub fn adjust_durations(&mut self) {
        let state = &mut *self.state.lock().unwrap();
        state.adjust_durations();
        state.interrupt();
        self.condvar.notify_one();
    }

    fn launch(&self){
        let lock = self.state.clone();
        let condvar = self.condvar.clone();
        let thread_alive = self.thread_alive;
        thread::spawn(move || {
            // eprintln!("spawn new timer thread");
            loop {
                // wait for the thread to start up
                let mut state = lock.lock().unwrap();
                let duration = match state.current_duration() {
                    Some(duration) => duration,
                    None => {
                        if thread_alive {
                            Duration::from_secs(std::u64::MAX)
                        }
                        else {
                            state.stop();
                            break;
                        }
                    },
                };

                loop {
                    if state.interrupted == true {
                        state.adjust_durations();
                        state.launch();
                        break;
                    }
                    // eprintln!("waiting for {:?}  {:?}", duration, SystemTime::now().duration_since(UNIX_EPOCH));
                    let result = condvar.wait_timeout(state, duration).unwrap();
                    state = result.0;
                    if result.1.timed_out() {
                        // eprintln!("timeout {:?} {:?}", duration, SystemTime::now().duration_since(UNIX_EPOCH));
                        let guard = state.queue.pop().unwrap();
                        let mut future_state = guard.lock().unwrap();
                        future_state.completed = true;
                        if let Some(waker) = future_state.waker.take() {
                            waker.wake()
                        }
                        state.adjust_durations();
                        break;
                    }

                }
            }
        });
    }
}

impl Clone for Timer {
    fn clone(&self) -> Self {
        Timer {
            state: self.state.clone(),
            condvar: self.condvar.clone(),
            thread_alive: self.thread_alive,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::executor::block_on;
    use std::time::Instant;
    use futures::join;

    #[test]
    fn test_timer(){
        let mut timer = Timer::new();


        let future = async {
            let benchmark = Instant::now();
            let future1 = timer.wait(Duration::from_millis(100));
            thread::sleep(Duration::from_millis(30));
            let future2 = timer.wait(Duration::from_millis(50));
            let future3 = timer.wait(Duration::from_millis(100));

            join!(future1, future2, future3);
            assert!(benchmark.elapsed() <= Duration::from_millis(140));
            let benchmark = Instant::now();
            let future1 = timer.wait(Duration::from_millis(100));
            thread::sleep(Duration::from_millis(30));
            let future2 = timer.wait(Duration::from_millis(50));
            let future3 = timer.wait(Duration::from_millis(100));
            join!(future1, future2, future3);
            let elapsed = benchmark.elapsed();
            dbg!(elapsed);
            assert!(elapsed <= Duration::from_millis(140));
        };
        block_on(future);
    }
}