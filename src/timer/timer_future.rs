use std::sync::{Arc, Mutex};
use std::task::{Poll, Context};
use std::future::Future;
use std::pin::Pin;
use std::time::Duration;
use crate::timer::timer_future_state::TimerFutureState;
use futures::future::FusedFuture;

pub type TimerFutureStateSafe = Arc<Mutex<TimerFutureState>>;

pub struct TimerFuture {
    shared_state: TimerFutureStateSafe,
    completed:bool,
}

impl TimerFuture {
    pub fn new(duration:Duration) -> TimerFuture {
        TimerFuture {
            shared_state: Arc::new(Mutex::new(TimerFutureState::new(duration))),
            completed: false,
        }
    }

    pub fn clone_state(&self) -> TimerFutureStateSafe{
        self.shared_state.clone()
    }

    pub fn set_max_duration(&self, duration:Duration){
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.duration > duration {
            shared_state.duration = duration;
        }
    }
}


impl Future for TimerFuture {
    type Output = ();
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // Look at the shared state to see if the timer has already completed.
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.completed {
            drop(shared_state);
            self.completed = true;
            Poll::Ready(())
        } else {
            shared_state.waker = Some(cx.waker().clone());
            Poll::Pending
        }
    }
}

impl FusedFuture for TimerFuture {
    fn is_terminated(&self) -> bool {
        self.completed
    }
}