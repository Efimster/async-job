use crate::timer::timer_future::{TimerFutureStateSafe, TimerFuture};
use std::time::{Instant, Duration};

pub struct TimerState {
    pub queue:Vec<TimerFutureStateSafe>,
    pub interrupted: bool,
    pub adjust_at: Option<Instant>,
}

impl TimerState {
    pub fn new() -> TimerState {
        TimerState {
            queue: Vec::new(),
            interrupted: false,
            adjust_at: None,
        }
    }

    pub fn interrupt(&mut self) {
        // eprintln!("interrupt");
        self.interrupted = true;
    }

    pub fn launch(&mut self) {
        // eprintln!("launch");
        self.interrupted = false;
        self.adjust_at = Some(Instant::now());
    }

    pub fn stop(&mut self) {
        // eprintln!("stop");
        self.interrupted = false;
        self.adjust_at = None;
    }

    pub fn add_to_queue(&mut self, duration: Duration) -> (TimerFuture, bool) {
        let index = self.get_new_index_by_duration(duration);
        let shortest = index == self.queue.len();

        let elapsed = self.elapsed();
        // eprintln!("add {:?} index={} shortest={} elapsed {:?}", duration, index, shortest, elapsed);
        let time_future = TimerFuture::new(duration + elapsed);
        self.queue.insert(index, time_future.clone_state());
        (time_future, shortest)
    }

    fn get_new_index_by_duration(&self, duration: Duration) -> usize {
        let elapsed = self.elapsed();
        for i in (0..self.queue.len()).rev() {
            let state = self.queue[i].lock().unwrap();
            if state.duration  > duration + elapsed {
                return i + 1;
            }
        }
        0
    }

    pub fn elapsed(&self) -> Duration {
        match self.adjust_at {
            Some(launched) => launched.elapsed(),
            None => Duration::new(0, 0),
        }
    }

    pub fn adjust_durations(&mut self) {
        self.decrement_durations(self.elapsed());
        self.adjust_at = Some(Instant::now());
    }

    pub fn decrement_durations(&mut self, duration:Duration) {
        // eprintln!("adjust duration {:?}", duration);
        for item in &mut self.queue {
            let mut item = item.lock().unwrap();
            item.decrement_duration(duration);
        }
    }

    pub fn is_launched(&self) -> bool {
        matches!(self.adjust_at, Some(_))
    }

    pub fn current_duration(&self) -> Option<Duration> {
        if self.queue.len() == 0 {
            None
        }
        else {
            Some(self.queue[self.queue.len() - 1].lock().unwrap().duration)
        }
    }
}