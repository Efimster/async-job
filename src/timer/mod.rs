pub mod timer;
pub mod timer_future;
mod timer_state;
mod timer_future_state;
#[cfg(target_arch = "wasm32")]
pub mod timer_wasm32;