use std::task::Waker;
use std::time::Duration;

pub struct TimerFutureState {
    /// Whether or not the sleep time has elapsed
    pub completed: bool,
    pub waker: Option<Waker>,
    pub duration: Duration,
}

impl TimerFutureState {
    pub fn new(duration:Duration) -> TimerFutureState {
        TimerFutureState {
            completed: false,
            waker: None,
            duration,
        }

    }

    pub fn decrement_duration(&mut self, duration:Duration) {
        if self.duration > duration {
            self.duration -= duration;
        } else {
            self.duration = Duration::new(0, 0);
        }
    }
}
