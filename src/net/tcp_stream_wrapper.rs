use mio::net::TcpStream as MIOTcpStream;
use crate::net::io_event_listener::IOEventListener;
use mio::Token;
use crate::common::ready_future::ReadyFuture;
use crate::common::ready_future_state::ReadyFutureResult;
use std::time::Duration;
use std::net::{TcpStream, ToSocketAddrs, SocketAddr, Shutdown};
use std::io;
use std::task::{Context, Poll};
use futures::{FutureExt, AsyncRead};
use std::io::{Read, Write};
use std::pin::Pin;

pub struct TcpStreamWrapper {
    tcp_stream: MIOTcpStream,
    io_event_listener: IOEventListener,
    token: Option<Token>,
    current_future: Option<ReadyFuture<()>>,
    pub read_timeout: Option<Duration>,
}

impl TcpStreamWrapper {
    pub fn from(tcp_stream: TcpStream, io_event_listener: IOEventListener) -> io::Result<TcpStreamWrapper> {
        tcp_stream.set_nonblocking(true)?;
        let tcp_stream = MIOTcpStream::from_std(tcp_stream);
        Ok(TcpStreamWrapper {
            tcp_stream,
            io_event_listener,
            token: None,
            current_future: None,
            read_timeout: None,
            // base_socket,
        })
    }

    pub fn connect<A: ToSocketAddrs>(addr: A, io_event_listener: IOEventListener) -> io::Result<TcpStreamWrapper> {
        Self::from(TcpStream::connect(addr)?, io_event_listener)
    }

    pub fn set_read_timeout(&mut self, dur: Option<Duration>) {
        self.read_timeout = dur;
    }

    pub fn local_addr(&self) -> io::Result<SocketAddr> {
        self.tcp_stream.local_addr()
    }

    pub fn peer_addr(&self) -> io::Result<SocketAddr> {
        self.tcp_stream.peer_addr()
    }

    pub fn shutdown(&self, how: Shutdown) -> io::Result<()>{
        self.tcp_stream.shutdown(how)
    }

    pub async fn read_until_data_available_async(&mut self) -> Result<Vec<u8>, io::Error> {
        let mut result = Vec::new();
        let mut buf = [0u8; 1500];
        let size = futures::AsyncReadExt::read(self, &mut buf).await?;
        result.extend_from_slice(&buf[..size]);
        loop {
            match io::Read::read(self, &mut buf) {
                Ok(size) =>
                    result.extend_from_slice(&buf[..size]),
                Err(err) if err.kind() == io::ErrorKind::WouldBlock =>
                    break,
                Err(err) => return Err(err),
            }
        }
        Ok(result)
    }

    fn register_for_event(&mut self) -> io::Result<()> {
        let (future, token) = self.io_event_listener.listen_read(
            &mut self.tcp_stream, self.read_timeout, self.token)?;
        self.token = Some(token);
        self.current_future = Some(future);
        Ok(())
    }

    pub fn poll_read_attempt(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<usize>> {
        if let Some(mut future) = self.current_future.take() {
            match future.poll_unpin(cx) {
                Poll::Pending => {
                    self.current_future = Some(future);
                    return Poll::Pending;
                },
                Poll::Ready(ReadyFutureResult::Timeout) =>
                    return Poll::Ready(Err(io::ErrorKind::WouldBlock.into())),
                Poll::Ready(_) => (),
            }
        }
        match io::Read::read(self, buf) {
            Ok(size) => {
                Poll::Ready(Ok(size))
            },
            Err(err) if err.kind() == io::ErrorKind::WouldBlock => {
                match self.register_for_event() {
                    Ok(_) => {
                        self.poll_read_attempt(cx, buf)
                    },
                    Err(err) => {
                        Poll::Ready(Err(err))
                    },
                }
            },
            Err(err) => Poll::Ready(Err(err)),
        }
    }
}

impl Read for TcpStreamWrapper {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        io::Read::read(&mut self.tcp_stream, buf)
    }
}

impl Write for TcpStreamWrapper {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.tcp_stream.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}


impl AsyncRead for TcpStreamWrapper {
    fn poll_read(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<usize>> {
        let me = self.get_mut();
        me.poll_read_attempt(cx, buf)
    }
}
