use mio::{Events, Interest, Poll, Token, event, Waker};
use std::io;
use std::collections::HashMap;
use crate::net::timeouts::Timeouts;
use std::time::Duration;
use crate::common::ready_future::{ReadyFutureStateSafe, ReadyFuture};

pub const MIO_WAKER_TOKEN: Token = Token(0);

pub(crate) struct IOEventListenerState {
    poll: Poll,
    pub map: HashMap<Token, ReadyFutureStateSafe<()>>,
    pub events: Events,
    next_token_index: usize,
    pub timeouts: Timeouts,
    pub launched: bool,
}

impl IOEventListenerState {
    pub fn new() -> io::Result<(IOEventListenerState, Waker)> {
        let poll = Poll::new()?;
        let waker = Waker::new(poll.registry(), MIO_WAKER_TOKEN)?;
        Ok((IOEventListenerState {
            poll,
            map: HashMap::new(),
            events: Events::with_capacity(128),
            next_token_index: MIO_WAKER_TOKEN.0 + 1,
            timeouts: Timeouts::new(),
            launched: false,
        }, waker))
    }

    pub fn listen_read<S>(&mut self, source: &mut S, timeout: Option<Duration>, token: Option<Token>) -> io::Result<(ReadyFuture<()>, Token)>
        where S: event::Source + ?Sized
    {
        let future = ReadyFuture::new();
        let token = match token {
            Some(token) => {
                // eprintln!("re-register {:?}", token);
                self.poll.registry().reregister(source, token, Interest::READABLE)?;
                token
            },
            None => {
                let token = self.next_token();
                // eprintln!("new-register {:?} timeout {:?}", token, timeout);
                self.poll.registry().register(source, token, Interest::READABLE)?;
                token
            },
        };
        self.map.insert(token, future.clone_state());
        if let Some(timeout) = timeout {
            // println!("add udp timeout {:?} {:?}", token, timeout);
            self.timeouts.add(token, timeout);
        }
        Ok((future, token))
    }

    pub fn is_listening(&self) -> bool {
        self.map.len() > 0
    }

    pub fn poll(&mut self) -> io::Result<Vec<(Token, bool)>>{
        self.timeouts.adjust_durations();
        let timeout = self.timeout();
        self.poll.poll(&mut self.events, timeout)?;
        let mut timeouts = self.timeouts.pop_timeouts();
        let mut result = Vec::new();
        for token in self.events.iter().map(|event| event.token()) {
            // eprintln!("event token {:?}", token);
            match timeouts.iter().position(|(timeout_token, _)|timeout_token.0 == token.0) {
                Some(position) => {
                    timeouts.remove(position);
                    result.push((token, true));
                },
                None => {
                    result.push((token, false));
                    if token != MIO_WAKER_TOKEN {
                        self.timeouts.remove(token);
                    }
                },
            }
        }
        for (token, _) in timeouts {
            result.push((token, true));
        }
        Ok(result)
    }

    fn next_token(&mut self) -> Token {
        let token = Token(self.next_token_index);
        self.next_token_index += 1;
        token
    }

    pub fn timeout(&self) -> Option<Duration> {
        match self.timeouts.current() {
            Some((_, duration)) => Some(duration),
            None => None,
        }
    }

    pub fn stop_listening<S>(&mut self, source: &mut S, token:Token) -> io::Result<()>
        where S: event::Source + ?Sized
    {
        self.map.remove(&token);
        self.poll.registry().deregister(source)?;
        self.timeouts.remove(token);
        Ok(())
    }

    pub fn set_timeout(&mut self, token:Token, timeout: Option<Duration>) {
        self.timeouts.remove(token);
        if let Some(duration) = timeout {
            self.timeouts.add(token, duration);
        }
    }

}

