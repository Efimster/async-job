use std::time::{Instant, Duration};
use mio::Token;

pub struct Timeouts {
    queue: Vec<(Token, Duration)>,
    pub adjust_at: Option<Instant>,
}

impl Timeouts {
    pub fn new() -> Timeouts {
        Timeouts {
            queue: Vec::new(),
            adjust_at: None,
        }
    }

    pub fn launch(&mut self) {
        // eprintln!("launch");
        self.adjust_at = Some(Instant::now());
    }

    pub fn stop(&mut self) {
        // eprintln!("stop");
        self.adjust_at = None;
        self.queue.clear();
    }

    pub fn add(&mut self, token:Token, duration: Duration) {
        let index = self.get_new_index_by_duration(duration);
        let shortest = index == self.queue.len();
        let elapsed = self.elapsed();
        // eprintln!("add {:?} index={} shortest={} elapsed {:?}", duration, index, shortest, elapsed);
        self.queue.insert(index, (token, duration + elapsed));
        if shortest {
            self.adjust_durations();
        }
        if self.len() == 1 {
            self.launch();
        }
    }

    pub fn remove(&mut self, token: Token) {
        match self.get_token_index(token) {
            Some(index) => {self.queue.remove(index);},
            None => (),//panic!("Try to not call timeouts.remove(token) if no such timeout exists"),
        }
    }

    fn get_index_by <F>(&self, func:F) -> Option<usize>
        where F: Fn((Token, Duration)) -> bool
    {
        for i in (0..self.queue.len()).rev() {
            let state = self.queue[i];
            if func(state) {
                return Some(i);
            }
        }
        None
    }

    fn get_new_index_by_duration(&self, duration: Duration) -> usize {
        let elapsed = self.elapsed();
        let result = self.get_index_by(|item| item.1 > duration + elapsed);
        match result {
            Some(index) => index + 1,
            None => 0,
        }
    }

    pub fn get_token_index(&self, token: Token) -> Option<usize> {
        self.get_index_by(|item| item.0 == token)
    }

    pub fn elapsed(&self) -> Duration {
        match self.adjust_at {
            Some(launched) => launched.elapsed(),
            None => Duration::new(0, 0),
        }
    }

    pub fn adjust_durations(&mut self) {
        self.decrement_durations(self.elapsed());
        self.adjust_at = Some(Instant::now());
    }

    pub fn decrement_durations(&mut self, duration: Duration) {
        // eprintln!("adjust duration {:?}", duration);
        for item in &mut self.queue {
            if item.1 > duration {
                item.1 -= duration;
            } else {
                item.1 = Duration::new(0, 0);
            }
        }
    }

    pub fn is_launched(&self) -> bool {
        matches!(self.adjust_at, Some(_))
    }

    pub fn current(&self) -> Option<(Token, Duration)> {
        if self.len() == 0 {
            None
        } else {
            Some(self.queue[self.len() - 1])
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.queue.len()
    }

    pub(crate) fn pop_timeouts(&mut self) -> Vec<(Token, Duration)> {
        if self.len() == 0 {
            return vec![];
        }
        let elapsed = self.elapsed();
        let index = match self.get_index_by(|item| item.1 > elapsed) {
            Some(index) => index + 1,
            None => 0,
        };

        let mut result = self.queue.split_off(index);
        result.reverse();
        if self.len() == 0 {
            self.stop();
        }
        result
    }
}

// cargo test --features net
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pop_timeouts() {
        let mut timeouts = Timeouts::new();
        assert!(!timeouts.is_launched());
        timeouts.add(Token(0), Duration::from_secs(1));
        assert!(timeouts.is_launched());
        timeouts.add(Token(1), Duration::from_secs(2));
        timeouts.add(Token(3), Duration::from_secs(3));

        timeouts.adjust_at = Some(timeouts.adjust_at.unwrap() - Duration::from_secs(2));
        let result = timeouts.pop_timeouts();
        assert_eq!(result.len(), 2);
        assert_eq!(result[0].0, Token(0));
        assert_eq!(result[1].0, Token(1));

        let result = timeouts.pop_timeouts();
        assert_eq!(result.len(), 0);
        assert!(timeouts.is_launched());

        timeouts.adjust_at = Some(timeouts.adjust_at.unwrap() - Duration::from_secs(1));
        let result = timeouts.pop_timeouts();
        assert_eq!(result.len(), 1);
        assert!(!timeouts.is_launched());
    }
}