use std::future::Future;
use std::io;
use std::net::SocketAddr;
use std::pin::Pin;
use std::task::{Context, Poll};
use futures::future::FusedFuture;
use crate::traits::transport::ITransport;

#[derive(Debug)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub struct ReadFromSocket<'a, T> {
    pub(crate) reader: &'a mut T,
    pub(crate) buf: &'a mut [u8],
}

impl<T> Unpin for ReadFromSocket<'_, T> {}

impl<'a, T> ReadFromSocket<'a, T> {
    pub(crate) fn new(reader: &'a mut T, buf: &'a mut [u8]) -> Self {
        ReadFromSocket { reader, buf }
    }
}

// impl Future for ReadFromSocket<'_, UdpSocketWrapper> {
//     type Output = io::Result<(usize, SocketAddr)>;
//
//     fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
//         let this = &mut *self;
//         this.reader.poll_read_attempt(cx, this.buf)
//     }
// }

impl<T:ITransport> Future for ReadFromSocket<'_, T> {
    type Output = io::Result<(usize, SocketAddr)>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = &mut *self;
        this.reader.poll_read_attempt(cx, this.buf)
    }
}

impl<T:ITransport> FusedFuture for ReadFromSocket<'_, T>{
    fn is_terminated(&self) -> bool {
        false
    }
}