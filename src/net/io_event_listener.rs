use mio::{Token, event, Waker as MIOWaker};
use std::{io, thread};
use std::sync::{Arc, Mutex};
use crate::net::io_event_listener_state::{IOEventListenerState, MIO_WAKER_TOKEN};
use std::time::Duration;
use crate::common::ready_future::ReadyFuture;
use crate::common::ready_future_state::ReadyFutureResult;

pub struct IOEventListener {
    state: Arc<Mutex<IOEventListenerState>>,
    mio_waker: Arc<MIOWaker>,
}

impl IOEventListener {
    pub fn new() -> io::Result<IOEventListener> {
        let (state, mio_waker) = IOEventListenerState::new()?;
        Ok(IOEventListener {
            state: Arc::new(Mutex::new(state)),
            mio_waker: Arc::new(mio_waker),
        })
    }

    pub fn listen_read<S>(&self, source: &mut S, timeout:Option<Duration>, token: Option<Token>) -> io::Result<(ReadyFuture<()>, Token)>
        where S: event::Source + ?Sized
    {
        self.mio_waker.wake()?;
        let state = &mut *self.state.lock().unwrap();
        let (future, token) = state.listen_read(source, timeout, token)?;
        if !state.launched {
            state.launched = true;
            self.launch();
        }
        Ok((future, token))
    }

    fn launch(&self) {
        let lock = self.state.clone();
        // println!("LAUNCHED {}", self_id);
        thread::spawn(move || {
            loop {
                let mut waked = false;
                let mut state = lock.lock().unwrap();
                if !state.is_listening() {
                    state.launched = false;
                    break;
                }
                loop { // loop to handle spurious wake-ups
                    if waked {

                        drop(state);
                        thread::sleep(Duration::from_millis(1));
                        //gives a solid chance for main thread to lock the mutex. drop(state) is not enough.
                        // condvar.wait_timeout(state, Duration::from_millis(1)).unwrap();
                        break;
                    }
                    let result = state.poll().expect("err: mio poll failed (");


                    for (token, timeout) in result {
                        match token {
                            MIO_WAKER_TOKEN => {
                                waked = true
                            },
                            token => {
                                let guard = match state.map.remove(&token) {
                                    Some(guard) => guard,
                                    None => continue,
                                };
                                let mut future_state = guard.lock().unwrap();
                                
                                future_state.result = Some(if timeout {
                                    ReadyFutureResult::Timeout
                                } else {
                                    ReadyFutureResult::Completed(())
                                });
                                if let Some(waker) = future_state.waker.take() {
                                    waker.wake();
                                    waked = true;
                                }
                            }
                        }
                    }
                    // eprintln!("poll waked {:?} {} {} active {}", &result, l1, l2, state.map.len());
                }
            };
            // println!("STOP LAUNCHED {} !!!", self_id);
        });
    }

    pub fn stop_listening<S>(&self, source: &mut S, token: Token) -> io::Result<()>
        where S: event::Source + ?Sized
    {
        self.mio_waker.wake()?;
        let state = &mut *self.state.lock().unwrap();
        state.stop_listening(source, token)?;
        // eprintln!("stop listening {:?} active {}", token, state.map.len());
        Ok(())
    }

    pub fn set_timeout(&self, token: Token, timeout: Option<Duration>) -> io::Result<()>{
        self.mio_waker.wake()?;
        let state = &mut *self.state.lock().unwrap();
        state.set_timeout(token, timeout);
        Ok(())
    }
}

impl Clone for IOEventListener {
    fn clone(&self) -> Self {
        IOEventListener {
            state: self.state.clone(),
            mio_waker: self.mio_waker.clone(),
        }
    }
}