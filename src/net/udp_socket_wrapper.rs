use std::io::{Read, Write};
use std::io;
use futures::{AsyncRead, AsyncWrite, FutureExt};
use std::task::{Context, Poll};
use std::pin::Pin;
use mio::net::UdpSocket as MIOUdpSocket;
use crate::net::io_event_listener::IOEventListener;
use mio::Token;
use std::net::{UdpSocket, ToSocketAddrs, SocketAddr};
use std::time::Duration;
use crate::common::ready_future::ReadyFuture;
use crate::common::ready_future_state::ReadyFutureResult;
use std::fmt::{Debug, Formatter, Error};
use crate::net::read_from_socket::ReadFromSocket;
use crate::traits::transport::ITransport;

pub struct UdpSocketWrapper {
    udp_socket: MIOUdpSocket,
    io_event_listener: IOEventListener,
    token: Option<Token>,
    current_future: Option<ReadyFuture<()>>,
    pub read_timeout: Option<Duration>,
    // base_socket: UdpSocket,
}

impl UdpSocketWrapper {
    pub fn from(udp_socket: UdpSocket, io_event_listener: IOEventListener) -> io::Result<UdpSocketWrapper> {
        udp_socket.set_nonblocking(true)?;
        // let base_socket = udp_socket.try_clone()?;
        let udp_socket = MIOUdpSocket::from_std(udp_socket);
        Ok(UdpSocketWrapper {
            udp_socket,
            io_event_listener,
            token: None,
            current_future: None,
            read_timeout: None,
            // base_socket,
        })
    }

    pub fn get_ref(&self) -> &MIOUdpSocket {
        &self.udp_socket
    }

    pub fn get_mut(&mut self) -> &mut MIOUdpSocket {
        &mut self.udp_socket
    }

    pub fn bind<A: ToSocketAddrs>(addr: A, io_event_listener: IOEventListener) -> io::Result<UdpSocketWrapper> {
        Self::from(UdpSocket::bind(addr)?, io_event_listener)
    }

    pub fn connect<A: ToSocketAddrs>(&self, addr: A) -> io::Result<()> {
        for addr in addr.to_socket_addrs()? {
            self.udp_socket.connect(addr)?;
        }
        Ok(())
    }

    pub fn bind_and_connect<A: ToSocketAddrs>(addr: A, to_addr: A, io_event_listener: IOEventListener) -> io::Result<UdpSocketWrapper> {
        let result = Self::bind(addr, io_event_listener)?;
        result.connect(to_addr)?;
        Ok(result)
    }

    pub fn send(&self, buf: &[u8]) -> io::Result<usize> {
        self.udp_socket.send(buf)
    }

    pub fn recv(&self, buf: &mut [u8]) -> io::Result<usize> {
        self.udp_socket.recv(buf)
    }

    pub fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)>{
        self.udp_socket.recv_from(buf)
    }

    pub fn recv_from_async<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadFromSocket<'a, UdpSocketWrapper> {
        ReadFromSocket::new(self, buf)
    }

    fn register_for_event(&mut self) -> io::Result<()>{
        let (future, token) = self.io_event_listener.listen_read(
        &mut self.udp_socket, self.read_timeout, self.token)?;
        self.token = Some(token);
        self.current_future = Some(future);
        Ok(())
    }
}

impl ITransport for UdpSocketWrapper {
    fn send_to(&mut self, buf: &[u8], target: SocketAddr) -> io::Result<usize> {
        self.udp_socket.send_to(buf, target)
    }

    fn set_read_timeout(&mut self, dur: Option<Duration>) -> io::Result<()> {
        self.read_timeout = dur;
        if let Some(token) = self.token {
            self.io_event_listener.set_timeout(token, dur)?;
        }
        Ok(())
    }

    fn recv_from_async<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadFromSocket<'a, UdpSocketWrapper> {
        ReadFromSocket::new(self, buf)
    }

    fn abort_recv(&mut self) {
        // println!("abort_recv {:?}", self.token);
        if let Some(token) = self.token {
            self.io_event_listener.stop_listening(&mut self.udp_socket, token)
                .expect("can't de-register from IO polling");
            self.token = None;
        }
    }

    fn local_addr(&self) -> io::Result<SocketAddr> {
        self.udp_socket.local_addr()
    }

    fn poll_read_attempt(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<(usize, SocketAddr)>> {
        if let Some(mut future) = self.current_future.take() {
            match future.poll_unpin(cx) {
                Poll::Pending => {
                    self.current_future = Some(future);
                    return Poll::Pending;
                },
                Poll::Ready(ReadyFutureResult::Timeout) =>
                    return Poll::Ready(Err(io::ErrorKind::WouldBlock.into())),
                Poll::Ready(_) => (),
            }
        }
        match self.recv_from(buf) {
            Ok((size, peer_address)) => {
                Poll::Ready(Ok((size, peer_address)))
            },
            Err(err) if err.kind() == io::ErrorKind::WouldBlock => {
                match self.register_for_event() {
                    Ok(_) => {
                        self.poll_read_attempt(cx, buf)
                    },
                    Err(err) => {
                        Poll::Ready(Err(err))
                    },
                }
            },
            Err(err) => Poll::Ready(Err(err)),
        }
    }
}


impl Read for UdpSocketWrapper {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.udp_socket.recv(buf)
    }
}

impl Write for UdpSocketWrapper {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.udp_socket.send(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl AsyncRead for UdpSocketWrapper {
    fn poll_read(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<usize>> {
        let me = self.get_mut();
        match me.poll_read_attempt(cx, buf){
            Poll::Pending => Poll::Pending,
            Poll::Ready(Ok((size, _))) => Poll::Ready(Ok(size)),
            Poll::Ready(Err(err)) => Poll::Ready(Err(err)),
        }
    }
}

impl AsyncWrite for UdpSocketWrapper {
    fn poll_write(self: Pin<&mut Self>, _cx: &mut Context<'_>, _buf: &[u8]) -> Poll<io::Result<usize>> {
        //TODO:check if Interest::READABLE event is raised for this
        unimplemented!()
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        unimplemented!()
    }

    fn poll_close(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        unimplemented!()
    }
}

impl Debug for UdpSocketWrapper {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "{:?}", self.udp_socket)
    }
}

impl Drop for UdpSocketWrapper {
    fn drop(&mut self) {
        // eprintln!("dropping {}", self.udp_socket.local_addr().unwrap());
        self.abort_recv();
    }
}

// impl Clone for UdpSocketWrapper {
//     fn clone(&self) -> Self {
//         let udp_socket = self.base_socket.try_clone().unwrap();
//         let base_socket = self.base_socket.try_clone().unwrap();
//         let udp_socket = MIOUdpSocket::from_std(udp_socket);
//         UdpSocketWrapper {
//             udp_socket,
//             io_event_listener: self.io_event_listener.clone(),
//             token: self.token,
//             current_future: None,
//             read_timeout: self.read_timeout,
//             base_socket
//         }
//     }
// }