use std::thread;
use std::sync::{Arc, Mutex, mpsc};
use crate::message::Message;

pub struct Worker {
    pub id: usize,
    pub thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    pub fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::Builder::new().name(id.to_string()).spawn(move || {
            loop {
                //calling smart box MutexGuard<Receiver<_>>.unwrap() moves ownership inside unwrap function
                // so the mutex gets released after unwrap function scope
                let message = receiver.lock().unwrap().recv().unwrap();
                match message {
                    Message::NewJob(job) => {
                        println!("Worker {} got a job; executing.", id);
                        job.call_box();
                    }
                    Message::Terminate => {
                        println!("Worker {} has been asked to terminate.", id);
                        break;
                    }
                }
                println!("Worker {} has finished", id);
            }
        }).unwrap();

        Worker {
            id,
            thread: Some(thread),
        }
    }
}