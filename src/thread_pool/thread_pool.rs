use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use crate::worker::Worker;
use crate::message::Message;


pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool {
            workers,
            sender,
        }
    }

    pub fn execute<F>(&self, f: F)
        where
            F: FnOnce() + Send + 'static
    {
        let job = Box::new(f);
        self.sender.send(Message::NewJob(job)).unwrap();
        
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        println!("Sending terminate message to all workers.");

        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        println!("Shutting down all workers.");

        for worker in &mut self.workers {
            println!("Shutting down worker {}", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;
    use std::time::Duration;

    // #[test]
    fn test_new_pool() {
        let t2;
        {
            let mut pool = ThreadPool::new(2);
            pool.execute(move || {
                thread::sleep(Duration::from_secs(2));
                println!(" xxxxx ")
            });
            pool.execute(move || { println!(" yyyyy ") });
            //let t1 = pool.workers[0].thread.take().unwrap();
            t2 = pool.workers[1].thread.take().unwrap();
            println!(" zzzz ");
            //t1.join();
            println!(" aaaaa ");

        }

        t2.join().unwrap();
        assert!(false);
    }

}


