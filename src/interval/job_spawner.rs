use futures::future::BoxFuture;
use crate::interval::job_result::JobResult;
use futures::channel::mpsc::Sender;

pub type Job<R, F> = BoxFuture<'static, JobResult<R, F>>;

#[derive(Clone)]
pub struct JobSpawner<R, F>{
    pub sender: Sender<Job<R, F>>,
}

impl<R, F> JobSpawner<R, F> {
    pub fn spawn(&mut self, job:Job<R, F>) {
        self.sender.try_send(job).expect("too many tasks queued");
    }
}