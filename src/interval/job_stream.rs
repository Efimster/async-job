use futures::{Stream, StreamExt};
use futures::stream::{FusedStream, FuturesUnordered};
use std::pin::Pin;
use std::time::Duration;
use crate::interval::job_result::JobResult;
use std::collections::VecDeque;
use core::task::{Context, Poll};
use crate::interval::interval_stream::IntervalStream;
use futures::channel::mpsc;
use crate::interval::job_spawner::{Job, JobSpawner};
use crate::timer::timer::Timer;

pub struct JobStream<R, F> {
    jobs: mpsc::Receiver<Job<R, F>>,
    interval_stream: IntervalStream,
    terminated: bool,
    running_jobs: FuturesUnordered<Job<R, F>>,
    scheduled_jobs: VecDeque<Job<R, F>>,
    // pub start: Instant,
}

impl<R, F> JobStream<R, F>
    where R: Unpin + 'static
{
    pub fn new(interval:Duration) -> (JobStream<R, F>, JobSpawner<R, F>) {
        Self::new_with_timer(interval, Timer::new_thread_alive())
    }

    pub fn new_with_timer(interval:Duration, timer: Timer) -> (JobStream<R, F>, JobSpawner<R, F>) {
        const MAX_QUEUED_TASKS: usize = 40;
        let (sender, receiver) = mpsc::channel(MAX_QUEUED_TASKS);
        let mut interval_stream = IntervalStream::with_timer(timer, interval);
        interval_stream.interrupt();
        (JobStream {
            terminated: false,
            jobs: receiver,
            interval_stream,
            running_jobs: FuturesUnordered::new(),
            scheduled_jobs: VecDeque::new(),
            // start: Instant::now(),
        }, JobSpawner {sender})
    }

    fn poll_interval_timer(&mut self, cx: &mut Context<'_>) -> Poll<Option<()>>{
        let result = self.interval_stream.poll_next_unpin(cx);
        if let Poll::Ready(Some(_)) = result {
            if let Some(job) = self.scheduled_jobs.pop_front().take() {
                self.running_jobs.push(job);
               // eprintln!("{:?} TA", self.start.elapsed());
            }

            if self.scheduled_jobs.len() == 0 {
                self.interval_stream.interrupt();
            }
        }

        result
    }

    fn poll_jobs(&mut self, cx: &mut Context<'_>) -> Poll<Option<()>> {
        loop {
            match self.jobs.poll_next_unpin(cx) {
                Poll::Pending => return Poll::Pending,
                Poll::Ready(Some(job)) => {
                    if self.interval_stream.interrupted {
                        self.running_jobs.push(job);
                        self.interval_stream.resume();
                    } else {
                        self.scheduled_jobs.push_back(job);
                    }
                },
                Poll::Ready(None) => return Poll::Ready(None),
            }
        }
    }

    fn poll_running_jobs(&mut self, cx: &mut Context<'_>) -> Poll<Option<JobResult<R, F>>> {
        loop {
            let result = self.running_jobs.poll_next_unpin(cx);
            if let Poll::Ready(Some(JobResult::Run{id:_, job})) = result {
                // eprintln!("{:?} Retry {}", self.start.elapsed(), id);
                self.scheduled_jobs.push_back(job);
                self.interval_stream.resume();
            }
            else {
                return result;
            }
        }

    }
}

impl<R, F> Stream for JobStream<R, F>
    where R: Unpin + 'static
{
    type Item = JobResult<R, F>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        const INPUT_STREAM_COUNT:usize = 3;
        while !self.terminated {
            let mut pending:usize = 0;

            if !self.interval_stream.is_terminated() {
                match self.poll_interval_timer(cx) {
                    Poll::Pending => {pending += 1;}
                    Poll::Ready(None) => (),
                    Poll::Ready(_) => (), //interval elapsed
                }
            }

            if !self.jobs.is_terminated() {
                match self.poll_jobs(cx) {
                    Poll::Pending => {pending += 1},
                    Poll::Ready(None) => (),
                    Poll::Ready(_) =>
                        panic!("poll_jobs should not return here. All available jobs should be scheduled"),
                }
            }

            if !self.running_jobs.is_terminated() {
                let result = self.poll_running_jobs(cx);
                match result {
                    Poll::Ready(Some(ref job_result)) => match job_result {
                        JobResult::Run { id:_, job:_ } =>
                            panic!("poll_running_jobs should not return here. Job should be rescheduled"),
                        _ => return result,
                    },
                    Poll::Ready(None) => (),
                    Poll::Pending => {pending += 1;}
                };
            }

            let terminated = self.interval_stream.is_terminated() as usize + self.jobs.is_terminated() as usize
                + self.running_jobs.is_terminated() as usize;

            if terminated == INPUT_STREAM_COUNT {
                self.terminated = true;
                break;
            }

            if pending == INPUT_STREAM_COUNT - terminated {
                return Poll::Pending;
            }
        }

        Poll::Ready(None)
    }
}



impl<R, F> FusedStream for JobStream<R, F>
    where R: Unpin + 'static
{
    fn is_terminated(&self) -> bool {
        self.terminated
    }
}

// impl<R> Sink<Job<R>> for JobStream<R>
// {
//     type Error = mpsc::SendError;
//
//     fn poll_ready(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         self.sink.poll_ready_unpin(cx)
//     }
//
//     fn start_send(mut self: Pin<&mut Self>, item: Job<R>) -> Result<(), Self::Error> {
//         self.sink.start_send_unpin(item)
//     }
//
//     fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         self.sink.poll_flush_unpin(cx)
//     }
//
//     fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         self.sink.poll_close_unpin(cx)
//     }
// }
