use futures::{Stream, FutureExt};
use std::task::{Context, Poll};
use std::pin::Pin;
use std::time::Duration;
use futures::stream::FusedStream;
use crate::timer::timer::Timer;
use crate::timer::timer_future::TimerFuture;

pub struct IntervalStream {
    interval: Duration,
    timer: Timer,
    current_future: TimerFuture,
    pub interrupted: bool,
}

impl IntervalStream {
    pub fn new(interval: Duration) -> IntervalStream {
        let timer = Timer::new_thread_alive();
        Self::with_timer(timer, interval)
    }

    pub fn with_timer(mut timer: Timer, interval: Duration) -> IntervalStream {
        let current_future = timer.wait(interval);
        IntervalStream {
            interval,
            timer,
            current_future,
            interrupted: false,
        }
    }

    pub fn interrupt(&mut self){
        self.interrupted = true;
        self.current_future.set_max_duration(Duration::from_secs(0));
        self.timer.adjust_durations();
        self.current_future = self.timer.wait(Duration::from_secs(u64::MAX));
    }

    pub fn resume(&mut self) {
        if self.interrupted {
            self.current_future.set_max_duration(self.interval);
            self.timer.adjust_durations();
        }
        self.interrupted = false;
    }

}

impl Stream for IntervalStream {
    type Item = ();

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if self.interrupted {
            return Poll::Pending;
        }

        match self.current_future.poll_unpin(cx) {
            Poll::Ready(item) => {
                let interval = if self.interrupted {
                    Duration::from_secs(u64::MAX)
                }
                else {
                    self.interval
                };
                self.current_future = self.timer.wait(interval);

                Poll::Ready(Some(item))
            },
            Poll::Pending => Poll::Pending
        }
    }
}

impl FusedStream for IntervalStream {
    fn is_terminated(&self) -> bool {
        self.interrupted
    }
}