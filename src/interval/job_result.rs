use crate::interval::job_spawner::Job;
use std::fmt::{Debug, Formatter};
use core::fmt;

pub enum JobResult<R, F> {
    Success { id: usize, result: R },
    Fail { id: usize, err: F },
    Run { id: usize, job: Job<R, F> },
    // Abort {id: usize, result: R},
}

impl<R, F> JobResult<R, F> {
    pub fn id(&self) -> usize {
        match self {
            JobResult::Success { id, result: _ } => *id,
            JobResult::Fail { id, err:_ } => *id,
            JobResult::Run { id, job: _ } => *id,
            // JobResult::Abort { id, result: _ } => *id,
        }
    }
}

impl<R: Debug, F: Debug> Debug for JobResult<R, F> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            JobResult::Success { id, result } => write!(f, "JobResult::Success id={} {:?}", id, result),
            JobResult::Fail { id, err } => write!(f, "JobResult::Failed id={} {:?}", id, err),
            JobResult::Run { id, job: _ } => write!(f, "JobResult::Run id={}", id),
            // JobResult::Abort { id, result } => write!(f, "JobResult::Abort id={} {:?}", id, result),
        }
    }
}