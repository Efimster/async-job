use crate::net::read_from_socket::ReadFromSocket;
use std::net::SocketAddr;
use std::io;
use std::task::{Context, Poll};
use crate::traits::transport::ITransport;
use std::time::Duration;
use std::io::Write;
use crate::timer::timer::Timer;
use futures::FutureExt;
use crate::timer::timer_future::TimerFuture;
use futures::io::ErrorKind;
use std::fmt::{Display, Formatter, Debug};
use core::fmt;
use std::sync::{Arc, Mutex};

pub type ReadBufItem = (Box<[u8]>, Option<Duration>);

pub struct UdpSocketWrapperMock {
    pub sent_result: io::Result<usize>,
    pub sent_bytes: Vec<Box<[u8]>>,
    read_timeout: Option<Duration>,
    read_buf: Box<dyn Iterator<Item=ReadBufItem>>,
    on_sent_bytes: Option<Arc<Mutex<dyn FnMut(&mut Self)>>>,
    pub local: SocketAddr,
    pub peer: SocketAddr,
    pub error: Option<io::ErrorKind>,
    timer: Option<Timer>,
    read_timeout_future: Option<TimerFuture>,
    delayed_read_item: Option<(TimerFuture, Box<[u8]>)>,
}

impl UdpSocketWrapperMock {
    pub fn new(local:SocketAddr, peer:SocketAddr) -> UdpSocketWrapperMock {

        UdpSocketWrapperMock {
            sent_result: Ok(0),
            sent_bytes:Vec::new(),
            read_timeout: None,
            read_buf: Box::new(Vec::new().into_iter()),
            on_sent_bytes: None,
            error: None,
            local,
            peer,
            timer: None,
            read_timeout_future: None,
            delayed_read_item: None,

        }
    }

    pub fn new_with_sent_listener<F>(local: SocketAddr, peer: SocketAddr, sent_listener:F)
        -> UdpSocketWrapperMock
        where F: FnMut(& mut Self ) + 'static
    {
        let mut result = UdpSocketWrapperMock::new(local, peer);
        result.set_sent_listener(sent_listener);
        result
    }

    pub fn push_read_bytes<I>(&mut self, read_buf:I)
        where I: IntoIterator<Item=ReadBufItem> +'static
    {
        let old_read_buf = std::mem::replace(&mut self.read_buf,
            Box::new(Vec::new().into_iter()));
        self.read_buf = Box::new(old_read_buf.chain(read_buf));
    }

    pub fn pending(&mut self, timeout:Option<Duration>) {
        self.set_read_timeout(timeout).unwrap();
        self.read_buf = Box::new(Vec::new().into_iter());
    }

    pub fn error(&mut self, error_kind:io::ErrorKind) {
        self.error = Some(error_kind);
    }

    fn fulfill(&mut self, mut buf:&mut [u8], bytes:&[u8]) -> io::Result<(usize, SocketAddr)> {
        match self.error {
            None => Ok((buf.write(bytes)?, self.peer)),
            Some(kind) => Err(kind.into()),
        }
    }

    pub fn get_last_sent_bytes(&self) -> Box<[u8]>{
        if self.sent_bytes.len() == 0 {
            return Box::new([]);
        }
        self.sent_bytes[self.sent_bytes.len() - 1].clone()
    }

    pub fn set_sent_listener<F>(&mut self, sent_listener:F)
        where F: FnMut(&mut Self) + 'static
    {
        self.on_sent_bytes = Some(Arc::new(Mutex::new(sent_listener)));
    }

    fn poll_read_attempt_empty_read_buf(&mut self, cx: &mut Context<'_>)
        -> Poll<io::Result<(usize, SocketAddr)>>
    {
        match self.read_timeout {
            None => Poll::Pending,
            Some(timeout) => {
                let mut fut = match self.read_timeout_future.take() {
                    None => self.timer.as_mut().unwrap().wait(timeout),
                    Some(fut) => fut,
                };
                match fut.poll_unpin(cx) {
                    Poll::Pending => {
                        self.read_timeout_future = Some(fut);
                        Poll::Pending
                    },
                    Poll::Ready(()) => {
                        Poll::Ready(Err(ErrorKind::WouldBlock.into()))
                    }
                }
            }
        }
    }

    fn poll_read_attempt_read_buf_item(&mut self, cx: &mut Context<'_>, buf: &mut [u8], item:ReadBufItem)
        -> Poll<io::Result<(usize, SocketAddr)>>
    {
        let (bytes, delay) = item;
        match delay {
            None => Poll::Ready(self.fulfill(buf, &bytes)),
            Some(duration) => {
                let fut = self.timer.as_mut().unwrap().wait(duration);
                self.poll_read_attempt_read_buf_delayed_item(cx, buf, (fut, bytes))
            }
        }
    }

    fn poll_read_attempt_read_buf_delayed_item(&mut self, cx: &mut Context<'_>, buf: &mut [u8],
        item: (TimerFuture, Box<[u8]>))
       -> Poll<io::Result<(usize, SocketAddr)>>
    {
        let (mut fut, bytes) = item;
        match fut.poll_unpin(cx) {
            Poll::Pending => {
                self.delayed_read_item = Some((fut, bytes));
                Poll::Pending
            },
            Poll::Ready(()) => Poll::Ready(self.fulfill(buf, &bytes)),
        }
    }
}

impl ITransport for UdpSocketWrapperMock {
    fn send_to(&mut self, buf: &[u8], target: SocketAddr) -> io::Result<usize> {
        self.peer = target;
        let result = match &self.sent_result {
            Ok(size) => {
                self.sent_bytes.push(Box::from(buf));
                Ok(*size)
            },
            Err(err) => Err(err.kind().into()),
        };
        if let Some( on_sent_bytes) = self.on_sent_bytes.clone() {
            let mut on_sent_bytes = on_sent_bytes.lock().unwrap();
            on_sent_bytes(self)
        }

        result
    }

    fn set_read_timeout(&mut self, dur: Option<Duration>) -> io::Result<()> {
        self.read_timeout = dur;
        if let Some(_) = dur {
            if let None = self.timer {
                self.timer = Some(Timer::new());
            } else {
                self.read_timeout_future = None;
            }
        }
        Ok(())
    }

    fn recv_from_async<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadFromSocket<'a, Self> where Self: std::marker::Sized {
        ReadFromSocket::new(self, buf)
    }

    fn abort_recv(&mut self) {}

    fn local_addr(&self) -> io::Result<SocketAddr> {
        Ok(self.local)
    }

    fn poll_read_attempt(&mut self, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<(usize, SocketAddr)>> {
        if let Some(delayed_read_item) = self.delayed_read_item.take() {
            return self.poll_read_attempt_read_buf_delayed_item(cx, buf, delayed_read_item)
        }
        match self.read_buf.next() {
            None => self.poll_read_attempt_empty_read_buf(cx),
            Some(item) => self.poll_read_attempt_read_buf_item(cx, buf, item),
        }
    }
}

impl Display for UdpSocketWrapperMock {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "udpMock({}->{}, timeout({:?}))",
               self.local, self.peer, self.read_timeout)
    }
}

impl Debug for UdpSocketWrapperMock {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

//  cargo test --features net
#[cfg(test)]
mod tests {
    use super::*;
    use std::net::{SocketAddrV4, Ipv4Addr};
    use futures::TryFutureExt;
    use futures::task::noop_waker_ref;
    use std::thread::sleep;
    use futures::executor::block_on;

    fn socket(num: usize) -> SocketAddr {
        SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(num as u8, num as u8, num as u8, num as u8), num as u16))
    }

    #[test]
    fn test_set_read_timeout() -> io::Result<()>{
        let mut udp = UdpSocketWrapperMock::new(socket(1), socket(2));
        let mut buf = [0u8; 1500];
        let waker = noop_waker_ref();
        let cx = &mut Context::from_waker(waker);
        let timeout = Duration::from_millis(5);
        udp.set_read_timeout(Some(timeout))?;
        let result = udp.recv_from_async(&mut buf).try_poll_unpin(cx);
        assert!(matches!(result, Poll::Pending));
        sleep(timeout + Duration::from_millis(2));
        let result = udp.recv_from_async(&mut buf).try_poll_unpin(cx);
        if let Poll::Ready(Err(err)) = result {
            assert_eq!(err.kind(), ErrorKind::WouldBlock);
        }
        else {
            panic!("expecting Poll::Ready(Err(err))");
        }

        Ok(())
    }

    #[test]
    fn test_sent_to() -> io::Result<()>{
        let on_sent_bytes = move |transport:&mut UdpSocketWrapperMock|{
            transport.push_read_bytes(vec![(transport.get_last_sent_bytes(), None)])
        };
        let mut udp = UdpSocketWrapperMock::new_with_sent_listener(socket(1),
            socket(2), on_sent_bytes);
        let data = [1u8; 150];
        udp.send_to(&data, socket(2))?;
        udp.push_read_bytes(std::iter::repeat((Box::from([1u8, 1, 1, 1]), None)));

        let mut buf = [0u8; 1500];
        let (len, _) = block_on(udp.recv_from_async(&mut buf))?;
        assert_eq!(len, 150);
        assert_eq!(&data[..], &buf[..len]);
        block_on(udp.recv_from_async(&mut buf))?;
        assert_eq!(&[1u8, 1, 1, 1], &buf[..4]);
        block_on(udp.recv_from_async(&mut buf))?;
        assert_eq!(&[1u8, 1, 1, 1], &buf[..4]);
        block_on(udp.recv_from_async(&mut buf))?;
        assert_eq!(&[1u8, 1, 1, 1], &buf[..4]);
        Ok(())
    }
}